# Lean Inception

## Introdução:
&emsp;&emsp; A Lean Inception representa uma metodologia ágil que mescla o melhor do Design Thinking, com seu foco no usuário e na inovação, e do Lean Startup, que prioriza a eficiência e a economia de recursos, para criar uma estratégia focada na definição e no desenvolvimento do Produto Mínimo Viável (MVP). Essa abordagem é realizada por meio de um workshop intensivo, geralmente conduzido ao longo de uma semana, dividido em várias sessões e atividades interativas.

Durante o workshop, equipes multidisciplinares trabalham juntas para identificar as necessidades dos usuários, definir os objetivos de negócio, gerar ideias e priorizar funcionalidades, culminando na elaboração de um plano de ação para o MVP. Esse processo não só auxilia na clareza do direcionamento do produto, mas também promove um alinhamento entre os membros da equipe, stakeholders e, eventualmente, os usuários finais.

O uso de técnicas visuais, brainstormings, mapeamento de jornadas do usuário e prototipagem rápida são alguns dos elementos-chave dessa metodologia, facilitando a comunicação e a compreensão mútua dentro da equipe. Ao final do workshop de Lean Inception, a equipe terá em mãos uma visão clara do caminho a seguir, permitindo iniciar o desenvolvimento do produto de forma ágil e alinhada às expectativas do mercado e dos usuários.





&emsp;&emsp; A princípio fizemos as seguintes dinâmicas no nosso Lean Inception:

- Visão do Produto;
- Produto É, NÁO É, FAZ, NÃO FAZ;
- Objetivos do produto;
- Personas;
- Jornada do Usuário
- Brainstorming de funcionalidades;
- Revisão técnica, de negócio e de UX
- Sequenciador;
- Canvas MVP

### Visão do produto

 &emsp;&emsp; A Visão do Produto é uma declaração que descreve o que o produto pretende alcançar e como ele impactará seus usuários ou o mercado. Ela articula o propósito fundamental do produto, seus principais benefícios e diferenciais, além de fornecer uma direção clara para a equipe de desenvolvimento. A visão deve ser inspiradora, concisa e facilmente compreensível por todos os membros da equipe e partes interessadas
 ___

![Visão do Produto](../assets/images/visaoDoProdutoT.png)

&emsp;&emsp; Para efetivamente realizar essa atividade, é recomendado adotar a seguinte sequência de passos:

1. Posicione o template da visão do produto em um local central, como um quadro branco ou flip chart, garantindo que seja facilmente acessível e visível a todos os participantes da equipe.

#### Resultado:
___
![Resultado Visão do Produto](../assets/images/visaoDoProduto.png)

### É Não É, Faz Não faz

&emsp;&emsp; Frequentemente, pode ser mais simples articular o que algo não é ou o que não realiza. A dinâmica "É - Não é - Faz - Não faz" (ENFN) é uma metodologia projetada para distinguir os atributos positivos e negativos de um produto, detalhando tanto suas funções e essência quanto suas ausências e limitações. Essa técnica tem como objetivo oferecer um entendimento detalhado e completo do produto, esclarecendo suas capacidades e restrições. Para colocar essa atividade em prática, siga os passos abaixo:

1. Crie quatro seções em um quadro branco ou flip chart, marcando-os como "É", "Não é", "Faz", e "Não faz".

2. Anote o nome do produto de forma destacada na parte superior, acima dos segmentos criados.

3. Peça aos membros da equipe que expressem suas percepções sobre o produto, anotando cada característica em post-its e alocando-os na seção correspondente.

4. Revise e organize os post-its em grupos de ideias similares.
___
![Resultado É Não É](../assets/images/ENaoET.png)

#### Resultado:
___
![Resultado É Não É](../assets/images/ENaoE.png)

### Objetivos do produto:

&emsp;&emsp; Todos os membros da equipe são encorajados a compartilhar sua visão sobre os objetivos do negócio, promovendo um ambiente onde diferentes perspectivas são exploradas para formar um entendimento comum sobre as prioridades. O propósito desta atividade é facilitar a identificação e o esclarecimento dos objetivos essenciais.

&emsp;&emsp;Segue o passo a passo para realizar a atividade:

1. Solicita-se que cada integrante da equipe reflita e anote, individualmente, três objetivos que eles consideram essenciais para o negócio em relação ao produto, respondendo à pergunta: "Quais seriam os três principais objetivos do produto para o negócio?".

2. Em seguida, todos os participantes apresentam suas ideias, colocando-as em um espaço compartilhado, como um quadro, e agrupando-as conforme a similaridade entre elas.

3. Com todos os objetivos compartilhados visualmente, a equipe trabalha em conjunto para refinar e consolidar esses objetivos. Durante esse processo, pode-se perceber que certas sugestões inicialmente propostas não se alinham com os verdadeiros objetivos do produto, sendo assim removidas. Este passo ajuda a esclarecer o direcionamento do projeto, proporcionando à equipe uma visão clara do que deve ser priorizado.
___
![Resultado Objetivos do Produto](../assets/images/objetivoT.png)

#### Resultado:
___
![Resultado Objetivos do Produto](../assets/images/objetivo.png)



### Personas:

&emsp;&emsp; Para realmente entender as funcionalidades de um produto, é essencial levar em conta os usuários finais e seus respectivos objetivos. Uma técnica amplamente utilizada para representar esses usuários é a criação de personas. Uma persona é uma representação fictícia de um usuário típico do produto ou serviço, englobando não somente seu papel específico, mas também suas necessidades e expectativas. Isso oferece uma visão mais concreta e tangível dos usuários, facilitando para a equipe o desenvolvimento de funcionalidades do produto que sejam verdadeiramente relevantes e valiosas para quem irá utilizá-lo.

&emsp;&emsp;Segue uma atividade prática para elaborar personas, caracterizada por ser direta, esclarecedora, divertida e eficiente:

1. Organize a equipe em duplas ou trios, instruindo cada grupo a esboçar uma persona.
2. Cada grupo deve, então, compartilhar a persona criada com o restante da equipe, oferecendo detalhes sobre quem ela é, suas necessidades e como o produto ou serviço se encaixa na sua vida.
3. Como um passo adicional e opcional, você pode incentivar os participantes a trocarem de grupos e repetirem os passos 1 e 2, enriquecendo assim a diversidade de perspectivas e compreensão das diferentes necessidades dos usuários.
___
![Persona 2](../assets/images/personaT.png)
#### Resultado:
___
![Persona 1](../assets/images/persona1.png)
___
![Persona 2](../assets/images/persona2.png)
___
![Persona 3](../assets/images/persona3.png)


### Jornadas do Usuário:
A Jornada do Usuário é uma representação visual das etapas que um usuário percorre ao interagir com o produto, desde a descoberta inicial até a utilização contínua e possíveis pontos de abandono. Ela mostra as experiências, emoções e pontos de contato do usuário durante sua interação com o produto. A análise da jornada do usuário ajuda a identificar oportunidades de melhoria e a entender melhor as necessidades dos usuários em diferentes estágios.


___
![Jornada do usuario](../assets/images/jornadaT.png)

#### Resultados:
___
![Jornada do usuario 1](../assets/images/jornada1.png)

<br>
___
![Jornada do usuario 2](../assets/images/jornada2.png)

### Brainstorming de funcionalidades:
  Uma funcionalidade é definida como qualquer ação que um usuário possa realizar com o produto, tal como emitir uma nota fiscal, acessar um extrato detalhado ou convidar amigos do Facebook. A descrição dessas funcionalidades deve ser clara e concisa, focada em cumprir um objetivo de negócio específico, satisfazer uma necessidade identificada da persona ou fazer parte de um momento específico na jornada do usuário.

Segue uma metodologia para facilitar um brainstorming sobre funcionalidades:

1. Solicite a um participante que leia, de forma pausada, os passos detalhados de uma jornada de usuário específica.
2. Enquanto a jornada é narrada, os demais membros do grupo são encorajados a sugerir possíveis funcionalidades que se alinham com cada etapa da jornada.
3. Ao surgir uma sugestão de funcionalidade, faça uma breve descrição dela e anote-a em um quadro visível a todos. Continue esse processo, repetindo os passos para cada jornada de usuário mapeada.

___
![Brainstorming de funcionalidades](../assets/images/stormT.png)

#### Resultados:
___
![Brainstorming de funcionalidades](../assets/images/storm1.png)
![Brainstorming de funcionalidades](../assets/images/storm2.png)
### Revisão técnica de negocios e de UX:
Essa revisão tem como propósito analisar o entendimento da equipe em relação aos aspectos técnicos, de negócio e de experiência do usuário (UX) para cada funcionalidade do produto. Através dessa atividade, pretende-se capturar novas perspectivas e tornar mais evidentes possíveis discordâncias e dúvidas.

Segue o processo para realizar essa revisão:

1. Seleciona-se uma pessoa para escolher uma funcionalidade e movê-la pelo gráfico e tabela preparados para essa finalidade.

2. Define-se uma cor que represente o nível de confiança da equipe em relação a essa funcionalidade, e então marcam-se os valores de negócio, esforço e valor de UX em uma escala de 1 a 3, utilizando símbolos como $, E e ♥, respectivamente.

3. Após a marcação, é importante confirmar se todos na equipe estão de acordo com a avaliação realizada. Em seguida, escolhe-se a próxima pessoa para repetir o processo a partir do passo 1. Esse método permite uma revisão abrangente das funcionalidades do produto, promovendo uma discussão colaborativa e a identificação de áreas onde há consenso ou discordância sobre o valor e a viabilidade de cada funcionalidade.
___
![Revisão tecnica](../assets/images/revisaoT.png)
___
![Revisão tecnica](../assets/images/revisaoT2.png)

#### Resultados:
___
![Revisão tecnica](../assets/images/revisao1.png)

### Sequenciador:
O Sequenciador é usada para priorizar e organizar as funcionalidades do produto em uma sequência lógica de implementação. Ele considera fatores como impacto no usuário, complexidade técnica, dependências e restrições de tempo e recursos. O sequenciamento ajuda a equipe a planejar e executar o desenvolvimento de forma eficiente, entregando valor ao usuário de maneira incremental.
___
![Sequenciador](../assets/images/seqT.png)

#### Resultados:
___
![Sequenciador](../assets/images/seq1.png)

### Canvas MVP
  O Canvas MVP é uma ferramenta visual projetada para auxiliar a equipe na definição e alinhamento da estratégia do Produto Mínimo Viável (MVP), que é a versão mais básica do produto capaz de atender às necessidades do negócio (produto mínimo) e ser validada pelo usuário final (produto viável).

Segue o procedimento para utilizar o Canvas MVP:

1. Divida a equipe em dois grupos e forneça a cada grupo o seu próprio template do Canvas MVP.
2. Solicite que cada grupo preencha o Canvas MVP com suas ideias e estratégias.
3. Após a conclusão, peça a cada grupo que apresente o seu Canvas MVP ao restante da equipe.
4. Conduza uma sessão de consolidação, na qual a equipe trabalha em conjunto para revisar e ajustar os sete blocos do Canvas MVP, utilizando e modificando as anotações feitas anteriormente conforme necessário. Este passo visa garantir que a estratégia geral do MVP seja coesa e aborde todas as áreas importantes do produto.
Essa abordagem colaborativa permite que a equipe compartilhe insights, perspectivas e conhecimentos específicos do domínio, resultando em um Canvas MVP mais robusto e alinhado com os objetivos do projeto.
___
![Canvas MVP](../assets/images/canvaT.png)

#### Resultados:
___
![Canvas MVP](../assets/images/canva1.png)


## Bibliografia

> CAROLI, P. Lean Inception. [s.l.] Editora Caroli, 2019.

## Histórico de versão
| Data | Versão | Descrição | Autor(es) |
| ---- | ---- | ---- | ---- |
| 04/04/2024 | 1.0 | Criação do Documento | Abdul Hannan , Heitor Marques , Hugo Rocha , Josue Tixeira |

