# Testes para Projeto de Desenvolvimento de Redes Neurais

## 1. Introdução

Este documento descreve os testes realizados no projeto de desenvolvimento de redes neurais para previsão do mercado forex. O objetivo dos testes é garantir a qualidade e a eficácia dos modelos desenvolvidos em cada etapa do projeto.

## 2. Testes da Etapa 1: Algoritmos Simples de Redes Neurais

### 2.1 Teste da Rede Neural Feedforward (FNN)

#### Objetivo do Teste:

Verificar se a FNN consegue aprender e fazer previsões precisas com base nos dados históricos do mercado forex.

#### Procedimento:

1. Treinar a FNN com dados históricos do mercado forex.
2. Avaliar o desempenho da FNN utilizando métricas de regressão, como MSE (Mean Squared Error) ou RMSE (Root Mean Squared Error).
3. Interpretar as previsões da FNN para entender seus pontos fortes e limitações.

#### Resultados Esperados:

Espera-se que a FNN seja capaz de capturar padrões nos dados do mercado forex e fazer previsões razoavelmente precisas.

### 2.2 Teste de Pré-processamento de Dados e Engenharia de Recursos

#### Objetivo do Teste:

Avaliar a eficácia das técnicas de pré-processamento de dados, como normalização e escalonamento de recursos, na melhoria do desempenho do modelo FNN.

#### Procedimento:

1. Aplicar técnicas de pré-processamento de dados aos dados históricos do mercado forex.
2. Treinar a FNN com os dados pré-processados.
3. Comparar o desempenho da FNN com e sem pré-processamento de dados.

#### Resultados Esperados:

Espera-se que o pré-processamento de dados melhore o desempenho da FNN, resultando em previsões mais precisas.

## 3. Testes da Etapa 2: Algoritmos Intermediários de Redes Neurais

### 3.1 Teste da Rede Neural Recorrente (RNN)

#### Objetivo do Teste:

Avaliar a capacidade da RNN em capturar dependências sequenciais nos dados do mercado forex e fazer previsões de curto prazo.

#### Procedimento:

1. Treinar a RNN com dados históricos do mercado forex.
2. Ajustar hiperparâmetros da RNN para otimizar seu desempenho.
3. Realizar testes retrospectivos (backtesting) da RNN em dados históricos para avaliar seu desempenho em um ambiente de negociação simulado.

#### Resultados Esperados:

Espera-se que a RNN capture com sucesso as dependências sequenciais nos dados do mercado forex e faça previsões precisas de curto prazo.

## 4. Testes da Etapa 3: Algoritmos Avançados de Redes Neurais

### 4.1 Teste da Rede Neural Convolucional (CNN)

#### Objetivo do Teste:

Explorar a capacidade da CNN em capturar padrões espaciais e temporais nos dados do mercado forex.

#### Procedimento:

1. Treinar a CNN com dados históricos do mercado forex.
2. Avaliar o desempenho da CNN na detecção de anomalias ou na negociação de alta frequência.
3. Comparar o desempenho da CNN com outros modelos desenvolvidos nas etapas anteriores do projeto.

#### Resultados Esperados:

Espera-se que a CNN seja capaz de capturar padrões complexos nos dados do mercado forex, melhorando as estratégias de negociação.

## 5. Conclusão

Os testes realizados ao longo do projeto ajudam a garantir a qualidade e a eficácia dos modelos de redes neurais desenvolvidos para previsão do mercado forex. Os resultados dos testes são utilizados para iterar e aprimorar continuamente os modelos, visando melhores resultados de previsão e desempenho de negociação.



## 9.Versionamento

|    Data    | Versão |      Descrição       |                   Autor(a)                    |                   Revisor(a)                    |
| ---------- | ------ | -------------------- | --------------------------------------------- | ----------------------------------------------- |
| 06/04/2024|  0.1   |     criação do documento      | [Abdul Hannan](https://gitlab.com/hannanhunny01) | [heitor marques]()|
