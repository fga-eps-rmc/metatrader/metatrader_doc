# Backlog

## Introdução:
Um backlog é uma lista organizada de tarefas, funcionalidades ou requisitos a serem realizados em um projeto ou produto.
Ele serve como uma fonte centralizada de trabalho prioritizado, fornecendo transparência sobre as atividades a serem executadas. O backlog é ajustado conforme necessário para refletir mudanças de prioridade ou novas informações, permitindo que as equipes respondam de forma ágil às demandas do projeto ou produto.

### Link da plataforma utilizada para backlog:

<iframe width="768" height="432" src="https://miro.com/app/live-embed/uXjVKW9uomU=/?moveToViewport=-2128,-884,4144,2163&embedId=637429223964" frameborder="0" scrolling="no" allow="fullscreen; clipboard-read; clipboard-write" allowfullscreen></iframe>

## Histórico de versão
| Data | Versão | Descrição | Autor(es) |
| ---- | ---- | ---- | ---- |
| 15/04/2024 | 1.0 | Criação do Documento | Abdul Hannan , Heitor Marques , Hugo Rocha , Josue Tixeira |
