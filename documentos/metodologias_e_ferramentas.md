# Metodologias e Ferramentas


## 1. Introdução
Este documento visa apresentar as abordagens metodológicas e as ferramentas selecionadas para a gestão e execução do projeto de desenvolvimento de software. O objetivo é garantir eficiência, transparência e flexibilidade ao longo do processo de trabalho.


### 1.1 Scrum


O Scrum e o Kanban são metodologias ágeis amplamente adotadas no desenvolvimento de software, visando melhorar a eficiência e a colaboração da equipe. O Scrum, assemelhando-se a uma corrida de revezamento, organiza o trabalho em ciclos denominados "sprints", com duração típica de uma a quatro semanas. Cada sprint é precedido por uma reunião de planejamento, seguida por reuniões diárias para acompanhamento do progresso e concluído com revisões e retrospectivas. Os papéis de Scrum Master, Product Owner e Scrum Team são fundamentais para seu funcionamento, juntamente com artefatos como Product Backlog, Sprint Backlog e Gráfico de Burndown.


Sendo assim, o Scrum surge para dividir o projeto em pequenas etapas, onde cada etapa possui um ciclo que deve ser seguido: Planejar, Construir, Testar e Revisar.

Existem 3 papéis essenciais para o trabalho funcionar:
* Scrum Master
* Product Owner
* Scrum Team (desenvolvedores, testadores, etc, ...)

E 3 artefatos que fazer parte da documentação:
* Product Backlog
* Sprint Backlog - São ditadas atravês de histórias de usuário.
* Gráfico de Burndown

### 1.2 Kanban

O Kanban, por sua vez, é comparável a uma linha de produção, onde o trabalho flui de maneira contínua e é visualizado em um quadro dividido em colunas representando os estágios do processo. Os cartões de tarefa movem-se pelo quadro conforme avançam nos estágios, sem a imposição de sprints fixos. O foco está em limitar o trabalho em progresso para manter um fluxo constante de entrega.



### 1.3 **XP**
A metodologia XP destaca-se pela valorização do bem-estar da equipe e pela qualidade do produto. Práticas como pair programming, escrita de testes, refatoração e proximidade com o cliente são fundamentais. O XP promove uma abordagem iterativa e incremental para o desenvolvimento de software.



## 2 **Escolha da(s) metodologia(s)**

Optou-se por integrar as metodologias Scrum, Kanban e XP, combinando suas melhores práticas para atender às necessidades do projeto "Back Game as a Service". A integração busca minimizar a complexidade desnecessária e aproveitar as vantagens de cada metodologia.



### 2.1 **SCRUM**
O Scrum foi selecionado devido à sua estrutura de sprints, facilitando o controle de prazos e entregas. As reuniões de Sprint Planning e Sprint Review ocorrerão semanalmente, com foco na definição de atividades e na revisão do progresso da equipe.


* **Sprint Planning:** Realizado nas terças ou quintas para planejar as atividades da semana.

* **Sprint Review:** Realizado nas terças ou quintas para checar o andamento da equipe com as atividades da semana.

* **Produto Backlog:** Lista de ideias e recursos que devem ser completados ao longo da semana.

### 2.2 **Kanban**
O Kanban será utilizado com checklists para acompanhar as atividades em um quadro dividido em colunas "To Do", "Doing" e "Done". Essa abordagem simples e eficaz promove a transparência e a gestão visual do fluxo de trabalho.


* **TO DO** - Atividades que ainda precisam ser realizadas.
* **Doing** - Atividades em andamento.
* **Done** - Atividades que foram finalizadas e aprovadas pela equipe.


* **Pair Programming(Programação em Pares):** Prática que ajuda a nivelar o conhecimento da equipe e garante códigos mais robustos, por ter dois pontos de vista alinhados.

* **Integração contínua:** Utilizaremos ferramentas de teste unitário para melhor manutenabilidade do software.

* **Padronização do código:** Todo código é desenvolvido seguindo um padrão, qualquer que seja, mas toda equipe deve seguir o mesmo padrão. Dessa forma, todos da equipe terão a mesma visão do código.


## 3. Ferramentas
3.1 Discord e Telegram
O Discord e o Telegram serão utilizados para comunicação interna da equipe, permitindo interações e discussões tanto formais quanto informais.

### 3.2 Figma
O Figma será empregado para o desenvolvimento de protótipos e documentos, oferecendo uma plataforma colaborativa e intuitiva.

### 3.3 Gitlab
O Gitlab servirá como repositório centralizado para armazenamento de códigos-fonte e documentos relacionados ao desenvolvimento do produto. Ele facilitará a colaboração, controle de versão e gerenciamento do trabalho da equipe.


## 4. Referências bibliográficas

> Deisy Braz S., COMBINAÇÃO DE MÉTODOS ÁGEIS NO PROCESSO DE DESENVOLVIMENTO DE SOFTWARE: UM ESTUDO DE CASO. Disponível em: <https://periodicos.uniarp.edu.br/index.php/ignis/article/download/1133/570/4664>

> SCRUM. Disponível em: <http://www.desenvolvimentoagil.com.br/scrum/>

> T.L., Métodos ágeis: o que são e como impactam o seu negócio? Disponível em: [https://www.lumis.com.br/a-lumis/blog/metodos-ageis...](https://www.lumis.com.br/a-lumis/blog/metodos-ageis.htm#:~:text=As%20metodologias%20%C3%A1geis%20s%C3%A3o%20uma,revistos%20com%20os%20m%C3%A9todos%20%C3%A1geis.)


## 9.Versionamento

|    Data    | Versão |      Descrição       |                   Autor(a)                    |                   Revisor(a)                    |
| ---------- | ------ | -------------------- | --------------------------------------------- | ----------------------------------------------- |
| 14/04/2024|  0.1   |     criação do documento      | [Abdul Hannan](https://gitlab.com/hannanhunny01) | [Josue Tiexeira]()|
