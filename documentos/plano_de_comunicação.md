# Plano de Comunicação

## 1. Objetivo

O objetivo deste plano de comunicação é estabelecer diretrizes claras para a comunicação eficaz entre todas as partes interessadas no projeto NeuralTrade. Isso inclui membros da equipe, partes interessadas internas, externas e outros envolvidos no projeto.

## 2. Partes Interessadas

### Equipe do Projeto
- Líder do Projeto: Abdul Hannan
- Desenvolvedores: Abdul Hannan, Heitor, Josué Teixeira

### Partes Interessadas Externas
- Clientes (Professor)

## 3. Canais de Comunicação

1. **Reuniões Presenciais:**
   - Reuniões que são realizadas durante a aula e eventuais reuniões que são marcadas com agendamento prévio

2. **Reuniões Remotas:**
   - O discord é a plataforma que é utilizada para realizar reuniões remotamente
   - Essas reuniões ocorrem semanalmente para alinhar as demandas da sprint atual.

4. **Plataforma de Gerenciamento de Projetos:**
   - O miro é a plataforma de gerenciamento de projetos utilizada para rastrear tarefas, atribuir responsabilidades e monitorar o progresso do projeto.
   - Todos os membros da equipe são responsáveis por manter suas tarefas atualizadas na plataforma.

## 4. Protocolos de Comunicação

1. **Atualizações de Status:**
   - Um relatório semanal para o professor, resumindo o progresso do projeto, problemas identificados e próximos passos.

## 5. Responsabilidades

Cada membro da equipe do projeto será responsável por garantir que as comunicações sejam claras, relevantes e oportunas. O líder do projeto supervisionará a implementação deste plano de comunicação e garantirá que todas as partes interessadas estejam informadas sobre o progresso do projeto.

