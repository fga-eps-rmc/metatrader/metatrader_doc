## Sobre NeuralTrade

O NeuralTrade é um projeto inovador que visa desenvolver um expert advisor (EA) altamente sofisticado e eficiente para análise e negociação automatizada nos mercados financeiros. Utilizando uma abordagem impulsionada pela inteligência artificial (IA) e deep learning, o NeuralTrade emprega técnicas avançadas de processamento de dados, análise preditiva e estratégias de negociação automatizada. Além disso, ele verifica o estado atual do mercado com diversos métodos, incluindo a análise de múltiplos sites através de modelos de processamento de linguagem natural (NLP) e reconhecimento de imagem para extrair dados de fontes variadas. Isso permite ao NeuralTrade oferecer uma solução abrangente e personalizável para traders de todos os níveis de experiência, fornecendo previsões robustas e sólidas.







## Contribuir
Para contribuir com esse projeto é importante seguir nosso [Guia de Contribuição](https://) do repositório e seguir nosso [Código de Conduta]().

## Ambientes
- [Documentation](https://gitlab.com/fga-eps-rmc/neuraltrade/neuraltrade_doc)


## Integrantes

### 2023-1

| Matricula | Nome | Gitlab | E-mail |
|-----------|------|--------|--------|
| 202045624 | Abdul Hannan | hannanuhnny01|  |
| 202016462 | HEITOR MARQUES SIMÕES BARBOSA | heitormsb|  |
| 202029012 | JOSUE TEIXEIRA SANTANA | zjosuez|  |
| 180136925 | Hugo Rocha de Moura | hugorochaffs|  |

